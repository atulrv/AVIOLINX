// import React, { Component } from 'react';
// import {
//   Platform,
//   StyleSheet,
//   Text,
//   View
// } from 'react-native';

// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' +
//     'Cmd+D or shake for dev menu',
//   android: 'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });

// type Props = {};
// export default class App extends Component<Props> {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>
//           Welcome to React Native!
//         </Text>
//         <Text style={styles.instructions}>
//           To get started, edit App.js
//         </Text>
//         <Text style={styles.instructions}>
//           {instructions}
//         </Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });


// import React, { Component } from 'react';
// import { Provider } from 'react-redux';

// import store from './app/store'; //Import the store
// import Home from './app/components/home' //Import the component file

// export default class App extends Component {
//     render() {
//         return (
//             <Provider store={store}>
//                 <Home />
//             </Provider>
//         );
//     }
// }

// Main navigation container

import React, { Component } from 'react';
import { Dimensions, View, AsyncStorage } from 'react-native';
import PropTypes from 'prop-types';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { middleware } from './app/reduxHelper/reduxHelper';
  
import { initializeServices } from './app/services';
import reducer from './app/reducers';

import createNavigators from './app/navigators';
import createAppWithNavigationState from './app/navigators/appnavigator';

const { width, height } = Dimensions.get('window');

const loggerMiddleware = createLogger();

let AppWithNavigationState = null;

class App extends Component {
    static propTypes = {
        path: PropTypes.string.isRequired,
        actions: PropTypes.instanceOf(Array),
        params: PropTypes.instanceOf(Object),
    };
    static defaultProps = {
        params: undefined,
        actions: [],
    };

    constructor(props) {
        super(props);
        createNavigators(props.path);

        AppWithNavigationState = createAppWithNavigationState();

        const middlesWares = [
            thunkMiddleware, // lets us dispatch() functions
        ];

        if (process.env.NODE_ENV === 'development') {
            middlesWares.push(loggerMiddleware);
        }

        this.store = createStore(reducer(props.path, props.params), applyMiddleware(...middlesWares));
        // this.store = createStore(reducer(props.path, props.params), applyMiddleware(middleware));

        initializeServices(this.store.dispatch);

        this.state = {};

        for (let i = 0; i < props.actions.length; i += 1) {
            const action = props.actions[i];
            this.store.dispatch(action);
        }

    }

    render() {
        return ( <View style = {
                { flex: 1 }
            } >
            <Provider store = { this.store } >
            <AppWithNavigationState/>
            </Provider>
            </View >
        );
    }
}

export default App;