import { NavigationActions } from 'react-navigation';
import { getNavigatorForPath } from '../navigators';
import { createAppNavigator } from '../navigators/appnavigator'
// Hax for broken react-navigation
function getActionForPathAndParams(path, params) {
    
    let correctedPath = path;
    console.log('getActionForPathAndParams:', path)
    console.log('getActionForPathAndParams params:', params)

    if (params && path) {
        correctedPath = path.replace(/:(\w+)/g, w => params[w.slice(1, w.length)]);
        if (correctedPath !== path) {
            correctedPath += '/hax';
        }
    }
    const navigator = getNavigatorForPath(path);
    const action = createAppNavigator().router.getActionForPathAndParams(correctedPath, params || {});
    action.path = path;
    action.params = params;
    return action;
}

export const getNavigationParams = (parentRoute) => {
    console.log('getNavigationParams(parentRoute):', parentRoute)
    const { index, routes, params } = parentRoute;
    if (index >= 0 && routes) {
        return getNavigationParams(routes[index]);
    }
    
    console.log('getNavigationParams(params):', params)
    return params;
};

export const initNavigationWithPath = (path, params = {}) => {
    const action = getActionForPathAndParams(path, params);
    action.type = NavigationActions.INIT;
    return action;
};

export const showAlert = alertData => ({
    type: 'ALERT_SHOW',
    alertData,
});
export const hideAlert = () => ({
    type: 'ALERT_HIDE',
});

//--navigations for each screen
export const NavigateToSplashScreen = () => getActionForPathAndParams('SplashScreen');
export const NavigateToLicenceScreen = () => getActionForPathAndParams('Home');
export const NavigateToContactsScreen = () => getActionForPathAndParams('Contacts');
export const NavigateToFrequencyScreen = () => getActionForPathAndParams('Frequency');
export const NavigateToSearchScreen = () => getActionForPathAndParams('Search');
export const NavigateToMoreScreen = () => getActionForPathAndParams('More');
export const NavigateToPropogationScreen = () => getActionForPathAndParams('Propogation');
export const NavigateToHfTrainingScreen = () => getActionForPathAndParams('HfTraining');


export const navigateBack = (options) => {
    const action = NavigationActions.back(options);
    Object.assign(action, options);
    return action;
};

export const navigateBackToNative = () => ({
    type: 'NAVIGATE_BACK_TO_NATIVE',
});

export const navigateBackToReactPage = () => ({
    type: 'Navigation/BACK',
});

export const navigateToNativePath = (path, params) => ({
    type: 'NAVIGATE_TO_NATIVE_PATH',
    path,
    params,
});

export const navigateToAndClear = action => ({
    ...action,
    type: 'NAVIGATE_AND_CLEAR',
});

export const setNextAction = nextAction => ({
    type: 'SET_NEXT_ACTION',
    nextAction,
  });
  
export const dispatchNextAction = (action) => {
    const actionType = 'DISPATCH_AND_CLEAR_NEXT_ACTION';
    const factory = (dispatch) => {
      dispatch(action);
      dispatch({
        type: actionType,
      });
    };
    factory.type = actionType;
    return factory;
};
