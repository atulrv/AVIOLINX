import React from 'react';
import {Text, View,Image} from 'react-native';

const  Header = (props) => {
    const {textStyle ,viewStyle,leftIcon} = styles;
    return (
        <View>
           {/* <View>
                <Image style={leftIcon} source={require('../images/back.png')} />
                <Text style={textStyle}>{props.headerText}</Text>
            </View>*/}
            <View >

                <View style={viewStyle}>
                    <Image style={leftIcon} source={require('../images/back.png')} />
                <Text style={textStyle}>{props.headerText}</Text>
                </View>
            </View>
        </View>
    );
};

const styles = {
    viewStyle:{
        backgroundColor:'#6b7f80',
        justifyContent:'center',
        alignItems:'center',
        height:40,
        paddingTop:15,
        shadowColor:'#000',
        shadowOffset:{width:0,height:2},
        shadowOpacity:0.2,
        elevation:2,
        position:'relative'
    },
    textStyle:{
        fontSize:25,
        marginBottom:12,
        color:'#B8860B'
    },
    leftIcon:{
        marginTop:8,
        marginRight:350,
        padding:10,
        height:3,
        width:3,
        //fontSize:0,
        //marginBottom:12,
        color:'#B8860B'
    }
};

export default Header;
