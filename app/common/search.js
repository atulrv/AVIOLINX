//'use strict';

import React, {Component} from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator, ImageBackground, Image, TouchableHighlight, TouchableOpacity,
    TouchableWithoutFeedback, Platform, BackHandler, ToastAndroid, TextInput, Keyboard
} from 'react-native';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions'; //Import your actions
import Header from '../common/header';
import {NavigateToContactsScreen, NavigateToMoreScreen, NavigateToLicenceScreen} from '../actions/navigation';
import ActionBar from 'react-native-action-bar';
import Overlay from 'react-native-modal-overlay';
import MyMenu from '../components/mymenu';
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';

class Search extends Component {
    constructor(props) {
        super(props);

        /*this.state = {
            modalVisible:false,
            visible:false
        };*/

        this.state = {
            text: '',
            startLoader:false
        };

    }

    _onChangeValue = (event) => {
        console.log("event",event)

        if(event === ''){
            this.state.startLoader = false;
           this.forceUpdate();
        }else {
            this.state.startLoader = true;
            this.forceUpdate();

        }
    };

    _onBackButton = () => {
        Keyboard.dismiss();
        setTimeout(function () {
            this.props.dispatch(NavigateToLicenceScreen());
        }.bind(this), 500)
    };


    render() {
        console.log("this.state.startloader", this.state.startLoader)
        return (
            <ImageBackground
                source={require('../images/plane.jpg')}
                style={styles.container}>
                <View>
                    {/* <Header headerText="Contacts"/> */}
                    <ActionBar
                        containerStyle={styles.bar}
                        title={'Search'}
                        disableShadows={true}
                        leftIconName={'back'}
                        onLeftPress={() => this._onBackButton()}
                        // onTitlePress={() => console.log('Title!')}
                        /*rightIcons={[
                            {
                                image: require('../images/more.png'),
                                onPress:() => this.toggleMenu(),
                            },
                        ]}*/
                    />

                    <Overlay visible={this.state.modalVisible}
                             closeOnTouchOutside animationType="zoomIn"
                        //containerStyle={{marginLeft:50}}
                             containerStyle={{backgroundColor: 'transparent'}}
                             childrenWrapperStyle={{
                                 backgroundColor: 'black',
                                 width: 120,
                                 marginLeft: 270,
                                 marginTop: -500
                             }}
                        //animationDuration={500}
                             onClose={() => this.toggleMenu()}
                    >
                        <MyMenu/>
                    </Overlay>
                </View>
                <View style={styles.container1}>
                    <View style={styles.SectionStyle}>
                        <Image source={require('../images/search.png')} style={styles.ImageStyle} />
                        <TextInput
                            style={{flex:1,fontSize:15,color:'#ffffff'}}
                            placeholder="Search Here"
                            placeholderTextColor='#0040ff'
                            underlineColorAndroid="transparent"
                            onChangeText={this._onChangeValue}
                            //onChangeText={(text) => this.setState({text})}
                            //value={this.state.text}
                        />
                    </View>
                    {
                        this.state.startLoader === true ?
                            <View style={styles.loader}>
                                {/* <ActivityIndicator size="large" color="#0080ff" />*/}
                                <Bars size={20} color="#ff4d4d" />
                                {/*<Pulse size={20} color="#52AB42" />*/}
                            </View>:
                            <Text style={styles.Pobox}>No Result Found !</Text>


                    }

                </View>
            </ImageBackground>
        );
    }


};


// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state

//Connect everything
export default connect()(Search);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: 'transparent',
    },

    title: {
        fontSize: 22,
        fontWeight: "600",
        color: '#FFFFFF',
        alignItems: 'center',
    },

    Pobox: {
        marginTop: 10,
        marginLeft: 150,
        fontSize: 15,
        fontWeight: "600",
        color: '#FFFFFF',
        alignItems: 'center',
    },

    bar: {
        //marginTop:20,
        backgroundColor: 'transparent',
        //marginLeft:50
    },
    container1: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        borderWidth: .5,
        borderColor: '#FFFF',
        height: 40,
        borderRadius: 25 ,
        margin: 10,
    },

    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    loader:{
        //flex: 1,
        //marginTop:-180,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80,
        //margin:-170
    }


});