//'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator,ImageBackground,Image,TouchableHighlight,TouchableOpacity ,TouchableWithoutFeedback,Platform,BackHandler,ToastAndroid
} from 'react-native';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../actions'; //Import your actions
import Header from '../common/header';

class MyMenu extends Component {
    constructor(props) {
        super(props);
       
        this.state = {
        };

    }

    
   
    render() {
            return (
                <ImageBackground
                source={require('../images/plane.jpg')}
                style={styles.container}>
                        <View>
                            <Header headerText="Contacts"/>
                        <Text style={styles.title}>
                            Contact
                       </Text>
                       <Text style={styles.title}>
                       7026644837
                       </Text>
                       <Text style={styles.title}>
                       7026644837
                       </Text>
                       <Text style={styles.title}>
                       7026644837
                       </Text>
                            </View>

                     
                        </ImageBackground>
            );
    }

   
};



// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state

//Connect everything
export default connect()(MyMenu);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor:'transparent',
      },
      Imagecontainer: {
        marginTop:20,
        backgroundColor:'transparent',
      },
    activityIndicatorContainer:{
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    button: {
        alignItems: 'center',
        padding: 10
      },
      Touchview: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:10,
        backgroundColor: 'black',
        height:130,
        width:130,
        opacity: 1,
      },
      TouchviewCenter: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:20,marginTop:-190,
        backgroundColor: 'black',
        height:80,
        width:80,
        borderRadius:50,
        borderColor:"grey",
        borderWidth:2

      },

    row:{
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10
    },

    title:{
        fontSize: 22,
        fontWeight: "600",
        color:'#B8860B',
        alignItems:'center',
    },

    description:{
        marginTop: 5,
        fontSize: 14,
        color:'#B8860B'
    }
});