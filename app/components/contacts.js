//'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator,ImageBackground,Image,TouchableHighlight,TouchableOpacity ,TouchableWithoutFeedback,Platform,BackHandler,ToastAndroid
} from 'react-native';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../actions'; //Import your actions
import Header from '../common/header';
import { NavigateToContactsScreen,NavigateToMoreScreen,NavigateToLicenceScreen} from '../actions/navigation';
import ActionBar from 'react-native-action-bar';
import Overlay from 'react-native-modal-overlay';
import MyMenu from './mymenu';

class Contacts extends Component {
    constructor(props) {
        super(props);
       
        this.state = {
            modalVisible:false
        };

    }

    toggleMenu = () => {
        this.setState({modalVisible: !this.state.modalVisible});

        setTimeout(function () {
            this.setState({modalVisible: !this.state.modalVisible});

        }.bind(this),2000)

    }

    _onBackButton= ()=>{
        this.props.dispatch(NavigateToLicenceScreen());
    };
    
   
    render() {
            return (
                <ImageBackground
                source={require('../images/plane.jpg')}
                style={styles.container}>
                        <View>
                            {/* <Header headerText="Contacts"/> */}
                            <ActionBar
                    containerStyle={styles.bar}
                    title={'Contacts'}
                    disableShadows={true}
                    leftIconName={'back'}
                    onLeftPress={() => this._onBackButton()}
                   // onTitlePress={() => console.log('Title!')}
                    rightIcons={[
                         {
                            image: require('../images/more.png'),
                            onPress:() => this.toggleMenu(),
                        },
                    ]}
                />
                <Overlay visible={this.state.modalVisible}
      closeOnTouchOutside animationType="zoomIn"
      //containerStyle={{marginLeft:50}}
      containerStyle={{backgroundColor: 'transparent'}}
      childrenWrapperStyle={{backgroundColor: 'black',width:120,marginLeft:270,marginTop:-500}}
      //animationDuration={500}
      onClose={() => this.toggleMenu()}
      >
        <MyMenu />
</Overlay>
                        <Text style={styles.title}>
                            Contact Us
                       </Text>
                            <Text style={styles.headOffice}>Head Office</Text>
                            <Text style={styles.headOffice}>Aviolinx</Text>
                            <Text style={styles.Pobox}>PO Box:1242</Text>
                            <Text style={styles.Pobox}>131 28 Nacka Strand</Text>
                            <Text style={styles.Pobox}>Swedan</Text>
                      {/* <Text style={styles.title}>
                       7026644837
                       </Text>
                       <Text style={styles.title}>
                       7026644837
                       </Text>
                       <Text style={styles.title}>
                       7026644837
                       </Text>*/}
                            </View>

                     
                        </ImageBackground>
            );
    }

   
};



// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state

//Connect everything
export default connect()(Contacts);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor:'transparent',
      },
      Imagecontainer: {
        marginTop:20,
        backgroundColor:'transparent',
      },
    activityIndicatorContainer:{
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    button: {
        alignItems: 'center',
        padding: 10
      },
      Touchview: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:10,
        backgroundColor: 'black',
        height:130,
        width:130,
        opacity: 1,
      },
      TouchviewCenter: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:20,marginTop:-190,
        backgroundColor: 'black',
        height:80,
        width:80,
        borderRadius:50,
        borderColor:"grey",
        borderWidth:2

      },

    row:{
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10
    },

    title:{
        fontSize: 22,
        fontWeight: "600",
        color:'#FFFFFF',
        alignItems:'center',
    },

    headOffice:{
        marginTop:5,
        fontSize: 15,
        fontWeight: "600",
        color:'#FFFFFF',
        alignItems:'center',
    },
    Pobox:{
        marginTop:10,
        fontSize: 15,
        fontWeight: "600",
        color:'#FFFFFF',
        alignItems:'center',
    },

    description:{
        marginTop: 5,
        fontSize: 14,
        color:'#B8860B'
    },
    bar: {
        //marginTop:20,
        backgroundColor:'transparent',
       //marginLeft:50
      },
});