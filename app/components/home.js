//'use strict';

import React, {Component} from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator,ImageBackground,Image,TouchableHighlight,TouchableOpacity ,TouchableWithoutFeedback, BackHandler, DeviceEventEmitter,Platform,ToastAndroid,Alert 
} from 'react-native';

import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
//import { NavigateToContactsScreen} from '../actions/navigation'
// import Header from '../common/header';
import ActionBar from 'react-native-action-bar';
import {
    NavigateToContactsScreen,
    NavigateToMoreScreen,
    NavigateToPropogationScreen,
    NavigateToHfTrainingScreen,
    NavigateToFrequencyScreen, NavigateToSearchScreen
} from '../actions/navigation'

import * as Actions from '../actions'; //Import your actions
import Overlay from 'react-native-modal-overlay';
import MyMenu from './mymenu';
import backAndroid, {
    hardwareBackPress,
    exitApp
  } from 'react-native-back-android';


class Home extends Component {
    constructor(props) {
        super(props);

        // this.state = {
        //     visible:false
        // };
        this.state = {visible: false, modalVisible: false, startLoader: false};
        //this.toggleMenu = this.toggleMenu.bind(this);

    }


    componentDidMount() {
        //this.props.getData(); //call our action


        console.log('Home Didmount');
        if (Platform.OS !== 'android') return
        BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);


        this.setState({startLoader: false});
    }

    toggleMenu= ()=> {
        this.setState({ modalVisible: !this.state.modalVisible });
        setTimeout(function(){
            this.setState({ modalVisible: !this.state.modalVisible });

        }.bind(this),2000)
      };

   
      

    timer = {
        ref: null,
        isTimerRunning: false
    };

    _handleBackPress = () => {


        const {navigation, dispatch} = this.props;
        console.log("this.props.navigation.state.routeName",this.props.navigation.state.routeName);
        console.log("this.props",this.props);
       
        // if (this.props.navigation.state.routeName === 'Home') { // exit the app from App page
            
        //     return this._handleExit();
          
        // } else { // in all the other cases, navigate back
        //     dispatch({ type: 'Navigation/BACK' })
        //     return true;
    
        // }
    

        console.log("Back pressed", dispatch);
        //navigation.goBack();
        // const activeRoute = navigation.routes[navigation.index];
        // if (activeRoute.index === 0) {
        //   //return false;
        //   return this._handleExit();
        // }
        //dispatch(NavigationActions.back());
         dispatch({ type: 'Navigation/BACK' })
         return true;
     }
     
     

    _handleExit = () => {


        if (!this.timer.isTimerRunning) {

            this.timer.isTimerRunning = true;

            const backInterval = 3000;

            clearTimeout(this.timer.ref);

            this.timer.ref = setTimeout(() => this.timer.isTimerRunning = false, backInterval);

            ToastAndroid.show('Press back again to exit the app', ToastAndroid.SHORT);

            return true; // don't do anything

        }

        return BackHandler.exitApp()
        return true;
    
    }

    componentWillUnmount() {
        
        console.log('Home WillUnmount')
        this.removeListener()
    }


    //--remove listener for backpress
    removeListener = () => {
        if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress')
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
        clearTimeout(this.timer.ref);

        this.timer = {
            ref: null,
            isTimerRunning: false
        };
    };

    _onPressButton = () => {
        this.setState({startLoader: true});

        setTimeout(function () {
            this.props.dispatch(NavigateToContactsScreen());
            this.setState({startLoader: false});
        }.bind(this),2000)
    };
    _onPressFrequency = () => {

        //this.startLoader = true;
        this.setState({startLoader: true});
        setTimeout(function () {
            this.props.dispatch(NavigateToFrequencyScreen());
            this.setState({startLoader: false});
        }.bind(this), 2000)
    };

    _onMoreCkicked = () => {
        this.setState({startLoader: true});

        setTimeout(function () {
            this.props.dispatch(NavigateToMoreScreen());
            this.setState({startLoader: false});
        }.bind(this),2000)
    };

    _onPropogationCkicked = () => {
        this.setState({startLoader: true});
        setTimeout(function () {
        this.props.dispatch(NavigateToPropogationScreen());
            this.setState({startLoader: false});
        }.bind(this), 2000)
    };

    _onhfCkicked = () => {
        this.setState({startLoader: true});
        setTimeout(function () {
            this.props.dispatch(NavigateToHfTrainingScreen());
            this.setState({startLoader: false});
        }.bind(this),2000)
    };

    _onRightCkicked = () => {
        alert('Menu');
    };


    // handleHardwareBackPress = () => {
    //     alert("hi");
    //     Alert.alert(
    //       'Quiting',
    //       'Want to quit?',
    //       [
    //         {
    //           text: 'Cancel',
    //           onPress: () => console.log('Cancel Pressed'),
    //           style: 'cancel'
    //         },
    //         { text: 'OK', onPress: () => exitApp() }
    //       ],
    //       { cancelable: false }
    //     );
    //     // return true to stop bubbling
    //     return true
    //   };

  /*  _onPressButton= ()=>{
        this.props.dispatch(NavigateToContactsScreen());
    

        setTimeout(function () {
            this.setState({modalVisible: !this.state.modalVisible});

        }.bind(this),2000)

    }*/

    render() {
        //console.log("startLoader",this.startLoader)

        return (
            <ImageBackground
                source={require('../images/plane.jpg')}
                style={styles.container}>
                {/* <Header headerText="Aviolinx"/> */}
                <ActionBar
                    containerStyle={styles.bar}
                    title={'Aviolinx'}
                    disableShadows={true}
                    leftIconName={''}
                    // onLeftPress={() => console.log('Left!')}
                    // onTitlePress={() => console.log('Title!')}
                    rightIcons={[
                        // {
                        //     name: 'star',
                        //     badge: '1',
                        //     onPress: () => console.log('Right Star !'),
                        // },
                        // {
                        //     name: 'phone',
                        //     badge: '1',
                        //     onPress: () => console.log('Right Phone !'),
                        //     isBadgeLeft: true,
                        // },
                        // {
                        //     name: 'plus',
                        //     onPress: () => console.log('Right Plus !'),
                        // },
                        {
                            image: require('../images/more.png'),
                            onPress: () => this.toggleMenu(),
                        },
                    ]}
                />

                <Overlay visible={this.state.modalVisible}
                         closeOnTouchOutside animationType="zoomIn"
                    //containerStyle={{marginLeft:50}}
                         containerStyle={{backgroundColor: 'transparent'}}
                         childrenWrapperStyle={{backgroundColor: 'black', width: 120, marginLeft: 270, marginTop: -500}}
                    //animationDuration={500}
                         onClose={() => this.toggleMenu()}
                >
                    <MyMenu/>
                </Overlay>

                <View style={{backgroundColor: 'black', height: 150, alignItems: 'center'}}>
                    <Image
                        source={require('../images/cloud.png')}
                        style={styles.Imagecontainer}
                    >
                    </Image>
                    <Text style={styles.title}>
                        Update and Download
                    </Text>
                    <Text style={styles.title}>
                        Propogation Tables
                    </Text>

                </View>
                <View style={{alignItems: 'center', marginTop: 50}}>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity activeOpacity={0.8} style={styles.Touchview}
                                          onPress={() => this._onPropogationCkicked()}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            />
                            <Text style={styles.description}>
                                Propogation Tab...
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={0.8} style={styles.Touchview}
                                          onPress={() => this._onhfCkicked()}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            />
                            <Text style={styles.description}>
                                HF Training
                            </Text>
                        </TouchableOpacity>
                    </View>


                    <View style={{flexDirection: 'row', padding: 10}}>
                        <TouchableOpacity activeOpacity={0.8} style={styles.Touchview} onPress={this._onPressFrequency}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            />
                            <Text style={styles.description}>
                                Frequencies
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={0.8} style={styles.Touchview} onPress={this._onPressButton}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            />
                            <Text style={styles.description}>
                                Contact
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter}
                                          onPress={this._onMoreCkicked}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            />
                            <Text style={styles.description}>

                                More...

                            </Text>
                        </TouchableOpacity>

                    </View>
                    <View>
                        {
                            this.state.startLoader === true ?
                                <View style={styles.loader}>
                                   {/* <ActivityIndicator size="large" color="#0080ff" />*/}
                                    <Bars size={20} color="#ff4d4d" />
                                    {/*<Pulse size={20} color="#52AB42" />*/}
                                </View>:
                                null


                        }
                    </View>
                </View>
            </ImageBackground>
        );
    }


};


// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {}
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
// function mapDispatchToProps(dispatch) {
//     return bindActionCreators(Actions, dispatch);
// }

//Connect everything
export default connect()(Home);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: 'transparent',
    },
    Imagecontainer: {
        marginTop: 20,
        backgroundColor: 'transparent',
    },
    bar: {
        //marginTop:20,
        backgroundColor: 'transparent',
        //marginLeft:50
    },
    activityIndicatorContainer: {
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    button: {
        alignItems: 'center',
        padding: 10
    },
    Touchview: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        backgroundColor: 'black',
        height: 130,
        width: 130,
        opacity: 1,
    },

    TouchviewCenter: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20, marginTop: -190,
        backgroundColor: 'black',
        height: 80,
        width: 80,
        borderRadius: 50,
        borderColor: "grey",
        borderWidth: 2

    },
    TouchviewDrop: {
        alignItems: 'center',
        //justifyContent:'center',
        //marginLeft:20,marginTop:-190,
        // backgroundColor: 'black',
        height: 20,
        width: 100,
        // borderRadius:10,
        borderColor: "#B8860B",
        borderBottomWidth: 1,
        // marginTop:-10

    },

    row: {
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10
    },

    title: {
        fontSize: 22,
        fontWeight: "300",
        color: '#B8860B',
        alignItems: 'center',
    },

    description: {
        marginTop: 5,
        fontSize: 14,
        color: '#B8860B'
    },
    descriptionDrop: {
        // marginTop: 5,
        fontSize: 14,
        color: '#B8860B'
    },
    loader:{
        //flex: 1,
        marginTop:-180,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80,
        //margin:-170
    }
});