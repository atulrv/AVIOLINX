//'use strict';

import React, {Component} from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator,
    ImageBackground,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Platform,
    BackHandler,
    ToastAndroid
} from 'react-native';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {NavigateToContactsScreen, NavigateToMoreScreen, NavigateToLicenceScreen} from '../actions/navigation'
import * as Actions from '../actions'; //Import your actions
import Header from '../common/header';
import ActionBar from 'react-native-action-bar';
import Overlay from 'react-native-modal-overlay';
import MyMenu from './mymenu';

class More extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false
        };

    }

    toggleMenu = () => {
        this.setState({modalVisible: !this.state.modalVisible});

        setTimeout(function () {
            this.setState({modalVisible: !this.state.modalVisible});

        }.bind(this), 2000)

    }

    _onBackButton = () => {
        this.props.dispatch(NavigateToLicenceScreen());
    }


    render() {
        return (
            <ImageBackground
                source={require('../images/plane.jpg')}


                style={styles.container}>
                {/* <Header headerText="Aviolinx"/> */}
                <ActionBar
                    containerStyle={styles.bar}
                    title={'More'}
                    disableShadows={true}
                    leftIconName={'back'}
                    onLeftPress={() => this._onBackButton()}
                    // onTitlePress={() => console.log('Title!')}
                    rightIcons={[
                        {
                            image: require('../images/more.png'),
                            onPress: () => this.toggleMenu(),
                        },
                    ]}
                />
                <Overlay visible={this.state.modalVisible}
                         closeOnTouchOutside animationType="zoomIn"
                    //containerStyle={{marginLeft:50}}
                         containerStyle={{backgroundColor: 'transparent'}}
                         childrenWrapperStyle={{backgroundColor: 'black', width: 120, marginLeft: 270, marginTop: -500}}
                    //animationDuration={500}
                         onClose={() => this.toggleMenu()}
                >
                    <MyMenu/>
                </Overlay>
                <View style={{flexDirection: 'column', alignItems: 'center', paddingTop: 30}}>
                    <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter} onPress={this._onMoreCkicked}>
                        <View style={{flexDirection: 'row', padding: 10}}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            >
                            </Image>
                            <Text style={styles.description}>

                                About Us
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter} onPress={this._onMoreCkicked}>
                        <View style={{flexDirection: 'row', padding: 10}}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            >
                            </Image>
                            <Text style={styles.description}>
                                Propogation Forecast
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter} onPress={this._onMoreCkicked}>
                        <View style={{flexDirection: 'row', padding: 10}}>
                            <Image
                                style={styles.facebook}
                                source={require('../images/facebook.png')}
                            >
                            </Image>
                            <Text style={styles.description}>
                                Facebook
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter} onPress={this._onMoreCkicked}>
                        <View style={{flexDirection: 'row', padding: 10}}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            >
                            </Image>
                            <Text style={styles.description}>
                                Website
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter} onPress={this._onMoreCkicked}>
                        <View style={{flexDirection: 'row', padding: 10}}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            >
                            </Image>
                            <Text style={styles.description}>
                                Twitter
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter} onPress={this._onMoreCkicked}>
                        <View style={{flexDirection: 'row', padding: 10}}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            >
                            </Image>
                            <Text style={styles.description}>
                                Linkedin
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewCenter} onPress={this._onMoreCkicked}>
                        <View style={{flexDirection: 'row', padding: 10}}>
                            <Image
                                style={styles.button}
                                source={require('../images/earth.png')}
                            >
                            </Image>
                            <Text style={styles.description}>
                                Weather Forecast
                            </Text>
                        </View>

                    </TouchableOpacity>

                </View>
            </ImageBackground>
        );
    }


};


// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state

//Connect everything
export default connect()(More);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: 'transparent',
    },
    Imagecontainer: {
        marginTop: 20,
        backgroundColor: 'transparent',
    },
    activityIndicatorContainer: {
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    button: {
        alignItems: 'center',
        padding: 10
    },
    facebook:{
      height:30,
      width:25
    },
    Touchview: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        backgroundColor: 'black',
        height: 130,
        width: 130,
        opacity: 1,
    },
    TouchviewCenter: {
        //alignItems: 'center',
        // justifyContent:'center',
        // marginLeft:20,marginTop:-190,
        backgroundColor: 'black',
        height: 50,
        width: 380,
        borderRadius: 25,
        borderColor: "#B8860B",
        borderWidth: 1,
        marginTop: 5
        // paddingTop:30

    },

    row: {
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10
    },

    title: {
        fontSize: 22,
        fontWeight: "600",
        color: '#B8860B',
        alignItems: 'center',
    },

    description: {
        marginTop: 5,
        fontSize: 14,
        color: '#B8860B',
        marginLeft: 20
    },
    bar: {
        //marginTop:20,
        backgroundColor: 'transparent',
        //marginLeft:50
    },
});