//'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator,ImageBackground,Image,TouchableHighlight,TouchableOpacity ,TouchableWithoutFeedback,BackHandler,Platform,ToastAndroid
} from 'react-native';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
//import { NavigateToContactsScreen} from '../actions/navigation'
// import Header from '../common/header';
import ActionBar from 'react-native-action-bar';
import { NavigateToContactsScreen,NavigateToMoreScreen,NavigateToPropogationScreen,NavigateToHfTrainingScreen,NavigateToSearchScreen} from '../actions/navigation';

import * as Actions from '../actions'; //Import your actions
import Overlay from 'react-native-modal-overlay';


class MyMenu extends Component {
    constructor(props) {
        super(props);

        // this.state = {
        //     visible:false
        // };
        this.state = { visible:false, modalVisible: false };
    //this.toggleMenu = this.toggleMenu.bind(this);

    }



    componentDidMount() {
        //this.props.getData(); //call our action
        console.log('Home Didmount')
        if (Platform.OS !== 'android') return
            BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);
       
    }

    timer = {
        ref: null,
        isTimerRunning: false
      }

    _handleBackPress = () => {

        const {nav, dispatch} = this.props;
        console.log("this.props.navigation.state.routeName",this.props.navigation.state.routeName);
        console.log("this.props",this.props.navigation);
        if (this.props.navigation.state.routeName === 'Home') { // exit the app from App page
            
           // return this._handleExit();
           dispatch({ type: 'Navigation/BACK' })
           return true;

        } else { // in all the other cases, navigate back
            dispatch({ type: 'Navigation/BACK' })
            return true;
    
        }
    
    }

    _handleExit = () => {

        if (!this.timer.isTimerRunning) {
    
          this.timer.isTimerRunning = true;
    
          const backInterval = 3000;
    
          clearTimeout(this.timer.ref);
    
          this.timer.ref = setTimeout(() => this.timer.isTimerRunning = false, backInterval);
    
          ToastAndroid.show('Press back again to exit the app', ToastAndroid.SHORT);
    
          return true; // don't do anything
    
        }
    
        return BackHandler.exitApp()
    
    }

    componentWillUnmount() {
        console.log('Home WillUnmount')
        this.removeListener()                                        
    }

   
    //--remove listener for backpress
    removeListener = () => {
        if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress')
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
        clearTimeout(this.timer.ref);

        this.timer = {
            ref: null,
            isTimerRunning: false
        }; 
    };

    _onPressButton= ()=>{
        this.props.dispatch(NavigateToContactsScreen());
    };

    _onMoreCkicked= ()=>{
        this.props.dispatch(NavigateToMoreScreen());
    };

    _onPropogationCkicked= ()=>{
        this.props.dispatch(NavigateToPropogationScreen());
    };

    _onSearchCkicked= ()=>{
        //this.setState({ visible:false,modalVisible: false});
        //this.forceUpdate()
        //setTimeout(function () {
            this.props.dispatch(NavigateToSearchScreen());

        //}.bind(this),1000)

    };

    _onRightCkicked = ()=>{
       alert('Menu');
    };

    toggleMenu= ()=> {
        alert("55555")
        this.setState({ modalVisible: !this.state.modalVisible });
      };

    render() {

            return (
             
        <View style={{ flexDirection:'column'}}>
                            <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewDrop} onPress={() => this._onPropogationCkicked()}>
                            <View style={{ flexDirection:'row'}}>
                                {/* <Image
                                    style={styles.button}
                                    source={require('../images/earth.png')}
                                >
                                </Image> */}
                                <Text style={styles.descriptionDrop}>
                                
                     Notifications
                        </Text>
                        </View>
                            </TouchableOpacity >
                            <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewDrop} onPress={() => this._onSearchCkicked()}>
                            <View style={{ flexDirection:'row'}}>
                                {/* <Image
                                    style={styles.button}
                                    source={require('../images/earth.png')}
                                >
                                </Image> */}
                                <Text style={styles.descriptionDrop}>
                     Search
                        </Text>
                        </View>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewDrop} onPress={() => this._onhfCkicked()}>
                            <View style={{ flexDirection:'row'}}>
                                {/* <Image
                                    style={styles.button}
                                    source={require('../images/earth.png')}
                                >
                                </Image> */}
                                <Text style={styles.descriptionDrop}>
                     Rate Now
                        </Text>
                        </View>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.8} style={styles.TouchviewDrop} onPress={() => this._onhfCkicked()}>
                            <View style={{ flexDirection:'row'}}>
                                {/* <Image
                                    style={styles.button}
                                    source={require('../images/earth.png')}
                                >
                                </Image> */}
                                <Text style={styles.descriptionDrop}>
                     Share Now
                        </Text>
                        </View>
                            </TouchableOpacity>
                        </View>


                        
            );
    }

   
};



// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        
    }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
// function mapDispatchToProps(dispatch) {
//     return bindActionCreators(Actions, dispatch);
// }

//Connect everything
export default connect()(MyMenu);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor:'transparent',
      },
      Imagecontainer: {
        marginTop:20,
        backgroundColor:'transparent',
      },
      bar: {
        //marginTop:20,
        backgroundColor:'transparent',
       //marginLeft:50
      },
    activityIndicatorContainer:{
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    button: {
        alignItems: 'center',
        padding: 10
      },
      Touchview: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:10,
        backgroundColor: 'black',
        height:130,
        width:130,
        opacity: 1,
      },
      
      TouchviewCenter: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:20,marginTop:-190,
        backgroundColor: 'black',
        height:80,
        width:80,
        borderRadius:50,
        borderColor:"grey",
        borderWidth:2

      },
      TouchviewDrop: {
        alignItems: 'center',
        //justifyContent:'center',
       //marginLeft:20,marginTop:-190,
       // backgroundColor: 'black',
        height:20,
        width:100,
       // borderRadius:10,
        borderColor:"#B8860B",
        borderBottomWidth:1,
       // marginTop:-10

      },

    row:{
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10
    },

    title:{
        fontSize: 22,
        fontWeight: "300",
        color:'#B8860B',
        alignItems:'center',
    },

    description:{
        marginTop: 5,
        fontSize: 14,
        color:'#B8860B'
    },
    descriptionDrop:{
       // marginTop: 5,
        fontSize: 14,
        color:'#B8860B'
    }
});