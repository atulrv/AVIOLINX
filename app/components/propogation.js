//'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator,ImageBackground,Image,TouchableHighlight,TouchableOpacity ,TouchableWithoutFeedback,BackHandler,Platform,ToastAndroid,WebView,Dimensions
} from 'react-native';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { NavigateToContactsScreen,NavigateToMoreScreen,NavigateToLicenceScreen} from '../actions/navigation'

import * as Actions from '../actions'; //Import your actions
import Header from '../common/header';
import ActionBar from 'react-native-action-bar';
import Overlay from 'react-native-modal-overlay';
import MyMenu from './mymenu';


class Propogation extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            check : false,
            isLoading:false,
            modalVisible:false
        };

    }

    

    componentDidMount() {
       
    }

    
    showSpinner = () => {
        console.log('Show Spinner');
        this.setState({ isLoading: true });
    };

    hideSpinner = () => {
        console.log('Hide Spinner');
        this.setState({ isLoading: false });
    };
    

    _onPressButton= ()=>{
        this.props.dispatch(NavigateToContactsScreen());
    };

    _onBackButton= ()=>{
        this.props.dispatch(NavigateToLicenceScreen());
    };

    Hour1= ()=>{
        this.setState({check: true});
    };
    toggleMenu = () => {
        this.setState({modalVisible: !this.state.modalVisible});

        setTimeout(function () {
            this.setState({modalVisible: !this.state.modalVisible});

        }.bind(this),2000)

    };

    render() {
        console.log("Home",this.props)
        if(this.state.check){
               
                    return(
                        <ImageBackground
                        source={require('../images/plane.jpg')}
                        style={styles.container}>
                        {/* <View style={{height:400}}> */}
                        <View style={{height:600}}>
                      <WebView
                         source={{uri: 'http://www.google.com/'}}
                         //style={{marginTop: 20}}
                         onLoadStart={() => (this.showSpinner())}
                         onLoad={() => (this.hideSpinner())}
                    
                      />
                       {this.state.isLoading && (
                          
                           <View style={{justifyContent:'center'}}>
          <ActivityIndicator
           style={{marginTop:-150 }}
            size="large"
          />
          </View>
         
        )}
                      </View>
                      </ImageBackground>
                    );

           }else {
            return (
                <ImageBackground
                source={require('../images/plane.jpg')}
                style={styles.container}>
                    {/* <Header headerText="Propagation"/> */}
                    <ActionBar
                    containerStyle={styles.bar}
                    title={'Propogation Tables'}
                    disableShadows={true}
                    leftIconName={'back'}
                    onLeftPress={() => this._onBackButton()}
                   // onTitlePress={() => console.log('Title!')}
                    rightIcons={[
                         {
                            image: require('../images/more.png'),
                            onPress:() => this.toggleMenu(),
                        },
                    ]}
                />
                <Overlay visible={this.state.modalVisible}
      closeOnTouchOutside animationType="zoomIn"
      //containerStyle={{marginLeft:50}}
      containerStyle={{backgroundColor: 'transparent'}}
      childrenWrapperStyle={{backgroundColor: 'black',width:120,marginLeft:270,marginTop:-500}}
      //animationDuration={500}
      onClose={() => this.toggleMenu()}
      >
        <MyMenu />
</Overlay>
                        <View style={{ alignItems:'center',marginTop:50}}>
                        <View style={{ flexDirection:'row'}}>
                            <TouchableOpacity activeOpacity={0.8} style={styles.Touchview} onPress={() => this.Hour1()}>
                                <Image
                                    style={styles.button}
                                    source={require('../images/earth.png')}
                                />
                                <Text style={styles.description}>
                      Hourly-Area 2
                        </Text>
                            </TouchableOpacity >
                            <TouchableOpacity activeOpacity={0.8} style={styles.Touchview} onPress={() => this.Hour1()}>
                                <Image
                                    style={styles.button}
                                    source={require('../images/earth.png')}
                                />
                                <Text style={styles.description}>
                                Hourly-Area 3
                        </Text>
                            </TouchableOpacity>
                        </View>
                       
        
                        
                       
                        </View>
                        </ImageBackground>
            );
        }
    }

   
};



// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        
    }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
// function mapDispatchToProps(dispatch) {
//     return bindActionCreators(Actions, dispatch);
// }

//Connect everything
export default connect()(Propogation);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor:'transparent',
      },
      Imagecontainer: {
        marginTop:20,
        backgroundColor:'transparent',
      },
    activityIndicatorContainer:{
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    button: {
        alignItems: 'center',
        padding: 10
      },
      Touchview: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:10,
        backgroundColor: 'transparent',
        height:130,
        width:130,
        opacity: 1,
        borderRadius:5,
        borderColor:"white",
        borderWidth:1
      },
      TouchviewCenter: {
        alignItems: 'center',
        justifyContent:'center',
       marginLeft:20,marginTop:-190,
        backgroundColor: 'black',
        height:80,
        width:80,
        borderRadius:50,
        borderColor:"white",
        borderWidth:2

      },

    row:{
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10
    },

    title:{
        fontSize: 22,
        fontWeight: "600",
        color:'#B8860B',
        alignItems:'center',
    },

    description:{
        marginTop: 5,
        fontSize: 14,
       // color:'#B8860B'
        color:'white'

    },
    bar: {
        //marginTop:20,
        backgroundColor:'transparent',
       //marginLeft:50
      },
});