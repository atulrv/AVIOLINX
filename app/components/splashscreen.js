import React, { Component } from 'react';
import { View, Text, Button,
    StyleSheet, TouchableOpacity, Image,
    Platform, BackHandler, Dimensions } from 'react-native';
import { connect } from 'react-redux'
import { NavigateToLicenceScreen, navigateToAndClear } from '../actions/navigation'
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight:5,
        paddingLeft: 5
    },
    splashImage: {
        alignSelf: 'center',
        
    },
});


class SplashScreen extends Component {

    componentDidMount() {
        //this.props.loadedsplash=true;
    }

    render() { 
        setTimeout (() => {
            // this.props.dispatch(NavigateToLicenceScreen())
            this.props.dispatch(navigateToAndClear(this.props.dispatch(NavigateToLicenceScreen())))
        }, 3000)
        console.log('splash this.props:', this.props)
        return (
        <View style = { styles.container } >
            <Image
                style={styles.splashImage}
                resizeMode='center'
                source={require('../images/planeold.jpg')}/> 
        </View>
        );
    }
}

export default connect()(SplashScreen);