import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';
import { addListener } from '../reduxHelper/reduxHelper';

import Home from '../components/home';
import Contacts from '../components/contacts';
import Frequency from '../components/frequency';
import More from '../components/more';
import Propogation from '../components/propogation';
import HfTraining from '../components/hftraining';
import SplashScreen from '../components/splashscreen';
import Search from '../common/search';

let navigatorInstance = null;

export const AppRoutes = {
    SplashScreen: {
        path: 'SplashScreen',
        screen: SplashScreen,
    },
    Home: {
        path: 'Home',
        screen: Home,
    },
    Contacts: {
        path: 'Contacts',
        screen: Contacts,
    },
    More: {
        path: 'More',
        screen: More,
    },
    Propogation: {
        path: 'Propogation',
        screen: Propogation,
    },
    HfTraining: {
        path: 'HfTraining',
        screen: HfTraining,
    },
    Frequency: {
        path: 'Frequency',
        screen: Frequency,
    },
    Search: {
        path: 'Search',
        screen: Search,
    },
};

export const createAppNavigator = (initialRouteName = null) => {
    if (initialRouteName !== null) {
        // create a new AppNavigator if initialRouteName is specified.
        navigatorInstance = StackNavigator(AppRoutes, {
            initialRouteName,
            navigationOptions: {
                header: null,
            },
            mode: 'card',
        });
    } else if (navigatorInstance === null) {
        // create a temporary AppNavigator if no initialRouteName is
        // specified and there is no navigatorInstance.
        return StackNavigator(AppRoutes, {
            navigationOptions: {
                header: null,
            },
            mode: 'card',
        });
    }
    return navigatorInstance;
};

function createAppWithNavigationState(initialRouteName = null) {
    const Navigator = createAppNavigator(initialRouteName);

    const AppWithNavigationState = ({ dispatch, navigation }) => ( 
        < Navigator navigation = { addNavigationHelpers({ dispatch, state: navigation, addListener }) } />
    );
    AppWithNavigationState.propTypes = {
        dispatch: PropTypes.func.isRequired,
        navigation: PropTypes.instanceOf(Object).isRequired,
    };
    return AppWithNavigationState;
}

const mapStateToProps = state => ({
    navigation: state.navigation,
});

export default (initialRouteName = null) => connect(mapStateToProps)(createAppWithNavigationState(initialRouteName));