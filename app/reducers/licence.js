import Immutable from 'immutable';
import { verifyAppLicence, verifyAppLicenceGetSuccess, verifyAppLicenceGetFailed } from '../actions/licence';

const VerifyAppLicenceStatus = Immutable.Record({
    response: null,
    errorResponse: null,
    loading: false,
    error: false,
});

const licence = (state = Immutable.OrderedMap(), action) => {
  let nextState;

  switch (action.type) {
    case verifyAppLicence().type:
      console.log('licecne reducer:', action)
      nextState = {};
      nextState = state.set('verifyAppLicenceStatus', new VerifyAppLicenceStatus({
                loading: true,
                }),
            );
      break;
      case verifyAppLicenceGetSuccess().type:
      console.log('licecne reducerSuccess:', action)
      nextState = {};
      nextState = state.set('verifyAppLicenceStatus', new VerifyAppLicenceStatus({
                response: action.verifiedLicenceJsonResponse,
                loading: false,
                }),
            );
      break;
    case verifyAppLicenceGetFailed().type:
      console.log('licecne reducerFailed:', action.verifiedErrorResponse)
      nextState = {};
      nextState = state.set('verifyAppLicenceStatus', new VerifyAppLicenceStatus({
                loading: false,
                errorResponse: action.verifiedErrorResponse,
                error: true
                }),
            );
      break;
    default:
      break;
  }
  return nextState || state;
};

export default licence;
