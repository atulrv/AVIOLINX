import Immutable from 'immutable';
import { verifyLogin, verifyLoginGetSuccess, verifyLoginGetFailed } from '../actions/login';

const VerifyLoginStatus = Immutable.Record({
    response: null,
    errorResponse: null,
    loading: false,
    error: false,
});

const login = (state = Immutable.OrderedMap(), action) => {
  let nextState;

  switch (action.type) {
    case verifyLogin().type:
      console.log('login reducer:', action)
      nextState = {};
      nextState = state.set('verifyLoginStatus', new VerifyLoginStatus({
                loading: true,
                }),
            );
      break;
      case verifyLoginGetSuccess().type:
      console.log('login reducerSuccess:', action)
      nextState = {};
      nextState = state.set('verifyLoginStatus', new VerifyLoginStatus({
                response: action.verifiedLoginSuccessResponse,
                loading: false,
                }),
            );
      break;
    case verifyLoginGetFailed().type:
      console.log('login reducerFailed:', action.verifiedErrorResponse)
      nextState = {};
      nextState = state.set('verifyLoginStatus', new VerifyLoginStatus({
                loading: false,
                errorResponse: action.verifiedLoginErrorResponse,
                error: true
                }),
            );
      break;
    default:
      break;
  }
  return nextState || state;
};

export default login;
